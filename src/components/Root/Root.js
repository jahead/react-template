import React from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { deepOrange500 } from 'material-ui/styles/colors';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Grid, Row, Col } from 'react-bootstrap';
import AppBar from 'material-ui/AppBar';

// import css from './Root.css';

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

const Root = () => (
  <BrowserRouter >
    <MuiThemeProvider muiTheme={muiTheme}>
      <Route path="/">
        <Grid>
          <div>
            <AppBar
              title="Title"
              iconClassNameRight="muidocs-icon-navigation-expand-more"
            />
            <Row className="show-grid">
              <Col xs={12} md={8}><code>{'Col xs={12} md={8}'}</code></Col>
              <Col xs={6} md={4}><code>{'Col xs={6} md={4}'}</code></Col>
            </Row>
          </div>
        </Grid>
      </Route>
    </MuiThemeProvider>
  </BrowserRouter>
);

export default Root;
