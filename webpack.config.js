/**
 * Requires
 */
const path = require('path');
const merge = require('webpack-merge'); // eslint-disable-line import/no-extraneous-dependencies
const webpack = require('webpack'); // eslint-disable-line import/no-extraneous-dependencies
const Parts = require('./webpack.parts'); // eslint-disable-line import/no-extraneous-dependencies
/**
 * Variables
 */
const PATHS = {
  src: path.join(__dirname, 'src'),
  dist: path.join(__dirname, 'dist'),
};

const PORT = process.env.PORT || 8080;
const HOST = process.env.HOST || 'localhost';

/**
 * Common Configuration
 */
const Common = merge([
  {
    context: PATHS.src,
    entry: [
      // The context property references the source directory and tells
      // webpack to begin there. `main` is just the key that references
      // the starting point of the application, `index.js`
      './index.js',

    ],
    output: {
      // `[name]` will be replaced with the key that references our
      // entry point inside the `entry` object. In this case it will
      // be `main`
      filename: 'js/[name].bundle.js',
      path: PATHS.dist,
    },
    module: {
      rules: [
        {
          // Regex pattern that matches any files with a .js or .jsx
          // file extension
          test: /\.jsx?$/,
          include: [path.join(__dirname, 'src')],
          // Exclude the node_modules folder from being transpiled
          exclude: /node_modules/,
          // Transform all .js and .jsx files to standard ES5 syntax
          // using the Babel loader
          use: 'babel-loader',
        },
      ],
    },
    plugins: [
      // Generates an index.html file template with
      // our bundled JavaScript injected into the bottom of the body
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        minChunks: (module) => {
          // This prevents stylesheet resources with the .css or .scss extension
          // from being moved from their original chunk to the vendor chunk
          if (module.resource && (/^.*\.(css|scss)$/).test(module.resource)) {
            return false;
          }
          return module.context && (module.context.indexOf('node_modules') !== -1 || module.context.indexOf('assets/js') !== -1);
        },
      }),
    ],
  },
]);

module.exports = function (env) { // eslint-disable-line func-names
  /**
   * Production Configuration
   */
  if (env === 'production') {
    return merge([
      Parts.POLYFILLS(),
      Common,
      Parts.HTML(path.join(PATHS.src, 'templates/index.html')),
      Parts.IMGS(),
      Parts.lintJS({ paths: PATHS.src }),
      Parts.OPTIMIZE(),
      Parts.CSS(env),
      Parts.VIDEOS(),
      Parts.DEBUG(env),
    ]);
  }
  /**
   * Develpment Configuration
   */
  return merge([
    Parts.POLYFILLS(),
    Parts.HMR(HOST, PORT),
    Common,
    Parts.devServer({
      host: HOST,
      port: PORT,
    }),
    Parts.HTML(path.join(PATHS.src, 'templates/index.html'), `http://${HOST}:${PORT}/`),
    Parts.IMGS(),
    Parts.lintJS({
      paths: PATHS.src,
      exclude: /node_modules/,
      options: {
        emitWarning: true,
      },
    }),
    Parts.CSS(env),
    Parts.VIDEOS(),
    Parts.DEBUG(env),
  ]);
};
